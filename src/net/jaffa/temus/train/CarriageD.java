package net.jaffa.temus.train;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;

public class CarriageD extends ACarriage
{
	public boolean siv, epBrake, compressor, atc, destinationBoard;
	
	public boolean occupied;
	
	public CarriageD(String number, boolean reversed, boolean occupied)
	{
		super(number, reversed);
		
		this.occupied = occupied;
		
		atc = false;
		siv = true;
		epBrake = true;
		compressor = true;
		destinationBoard = Program.random.nextBoolean();
	}
	
	
	@Override
	public void render(Bitmap target, float locationX, float locationY, boolean illuminated)
	{
		if (reversed)
		{
			target.drawStringSingle(locationX, locationY, false, occupied ? 0x90 : 0xA0, 0x91, 0x92, 0x92, 0x92, 0x92, 0x93, 0xA3);
		}
		else
		{
			target.drawStringSingle(locationX, locationY, false, 0xA1, 0x91, 0x92, 0x92, 0x92, 0x92, 0x93, occupied ? 0x94 : 0xA4);
		}
		
		Bitmap carInterior;
		carInterior = new Bitmap(54, 18);
		
		if (reversed)
		{
			carInterior.drawStringSingle(0.6F, -1.0F / 12.0F, false,
					number.charAt(0) - 0x20, number.charAt(1) - 0x20, number.charAt(2) - 0x20, number.charAt(3) - 0x20);
			target.drawBitmap(carInterior, (int) (locationX * 16) + 8, (int) (locationY * 24) + 2, illuminated);
		}
		else
		{
			carInterior.drawStringSingle(0.9F, -1.0F / 12.0F, false,
					number.charAt(0) - 0x20, number.charAt(1) - 0x20, number.charAt(2) - 0x20, number.charAt(3) - 0x20);
			target.drawBitmap(carInterior, (int) (locationX * 16) + 2, (int) (locationY * 24) + 2, illuminated);
		}
		
	}
}
