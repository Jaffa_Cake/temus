package net.jaffa.temus.train;

import net.jaffa.temus.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public abstract class ACarriage
{
	public String number;
	public boolean reversed;
	
	public boolean bogieBrake, aircon, doors, mainLights, emergencyLights;
	
	public List<Integer> faults;
	
	public ACarriage(String number, boolean reversed)
	{
		this.number = number;
		this.reversed = reversed;
		
		faults = new ArrayList<Integer>();
		
		bogieBrake = true;
		aircon = true;
		doors = true;
		mainLights = true;
		emergencyLights = true;
	}
	
	public abstract void render(Bitmap target, float locationX, float locationY, boolean illuminated);
}
