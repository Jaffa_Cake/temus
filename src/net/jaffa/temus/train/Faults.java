package net.jaffa.temus.train;

public class Faults
{
	public static String getFaultText(int code)
	{
		switch (code)
		{
			case 1:
				return "CH-FAULT-OCR(RESET)"; // XXX Guess
			case 7:
				return "CH-FAULT-PSLVD"; // XXX Guess
			case 11:
				return "HT-HTF-HB1(RESET)";
			case 21:
				return "EP-NOREL";
			case 33:
				return "SIV-LINELV";
			case 37:
				return "SIV-ACOUT";
			case 72:
				return "ACON-ACFLT2"; // XXX Guess
			case 88:
				return "SEC-COMP";
			
			default:
				return "???";
		}
	}
}
