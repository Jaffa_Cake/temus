package net.jaffa.temus.train;

import net.jaffa.temus.graphics.Bitmap;

public class CarriageN extends ACarriage
{
	public boolean motor1, motor2;
	
	public CarriageN(String number, boolean reversed)
	{
		super(number, reversed);
		
		motor1 = true;
		motor2 = true;
	}
	
	@Override
	public void render(Bitmap target, float locationX, float locationY, boolean illuminated)
	{
		target.drawStringSingle(locationX, locationY, false, 0xA1, 0x91, 0x92, 0x92, 0x92, 0x92, 0x93, 0xA3);
		
		Bitmap carInterior;
		carInterior = new Bitmap(60, 18);
		carInterior.drawStringSingle(0.9F, -1.0F / 12.0F, false,
				number.charAt(0) - 0x20, number.charAt(1) - 0x20, number.charAt(2) - 0x20, number.charAt(3) - 0x20);
		
		target.drawBitmap(carInterior, (int) (locationX * 16) + 2, (int) (locationY * 24) + 2, illuminated);
	}
}
