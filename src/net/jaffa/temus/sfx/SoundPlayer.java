package net.jaffa.temus.sfx;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;



public class SoundPlayer
{
	private static Clip beepClip;
	private static Clip backgroundClip;
	private static Clip buzzClip;

	public static synchronized void playBeep()
	{
		try
		{
			if (beepClip == null)
			{
				beepClip = AudioSystem.getClip();
				AudioInputStream inputStream = AudioSystem.getAudioInputStream(SoundPlayer.class.getResource("/wav/beep.wav"));
				beepClip.open(inputStream);
			}

			beepClip.setFramePosition(0);
			beepClip.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static synchronized void playBackground()
	{
		try
		{
			if (backgroundClip == null)
			{
				backgroundClip = AudioSystem.getClip();
				AudioInputStream inputStream = AudioSystem.getAudioInputStream(SoundPlayer.class.getResource("/wav/bg.wav"));
				backgroundClip.open(inputStream);
			}

			backgroundClip.setFramePosition(0);
			backgroundClip.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static synchronized void playBuzz()
	{
		try
		{
			if (buzzClip == null)
			{
				buzzClip = AudioSystem.getClip();
				AudioInputStream inputStream = AudioSystem.getAudioInputStream(SoundPlayer.class.getResource("/wav/buzz.wav"));
				buzzClip.open(inputStream);
			}

			buzzClip.setFramePosition(0);
			buzzClip.loop(-1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
