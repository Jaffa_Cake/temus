package net.jaffa.temus.graphics;


import net.jaffa.temus.Program;



public class Bitmap
{
	public final int width;
	public final int height;

	public int[] pixels;


	public Bitmap(int width, int height)
	{
		this.width = width;
		this.height = height;

		this.pixels = new int[width * height];
	}


	protected boolean alphaCheck(int top)
	{
		return (top & 0x00FFFFFF) != 0;
	}


	public Bitmap clear(int colour)
	{
		for (int i = 0; i < pixels.length; i++)
		{
			pixels[i] = colour;
		}

		return this;
	}

	public Bitmap drawBitmap(Bitmap bitmap, int offsetX, int offsetY, boolean inverted)
	{
		int targetX = 0;
		int targetY = 0;

		for (int x = 0; x < bitmap.width; x++)
		{
			for (int y = 0; y < bitmap.height; y++)
			{
				targetX = x + offsetX;
				targetY = y + offsetY;

				if (targetX < 0 || targetX >= this.width)
					continue;
				if (targetY < 0 || targetY >= this.height)
					continue;

				if (alphaCheck(bitmap.pixels[(y * bitmap.width) + x]) ^ inverted)
				{
					this.pixels[(targetY * this.width) + targetX] = Program.COLOUR;
				}
			}
		}

		return this;
	}


	public Bitmap drawBitmapScaled(Bitmap bitmap, int destinationX, int destinationY, int destinationWidth, int destinationHeight, int sourceX, int sourceY, int sourceWidth, int sourceHeight, boolean inverted)
	{

		Bitmap interim = new Bitmap(sourceWidth, sourceHeight);
		interim.drawBitmap(bitmap, -sourceX, -sourceY, false);

		int sourcePixel = 0x000000;
		int sourcePixelX = 0;
		int sourcePixelY = 0;
		for (int x = 0; x < destinationWidth; x++)
		{
			for (int y = 0; y < destinationHeight; y++)
			{

				if (destinationX + x < 0 || destinationX + x >= this.width)
					continue;
				if (destinationY + y < 0 || destinationY + y >= this.height)
					continue;

				sourcePixelX = (int)(((float)x / (float)destinationWidth) * (float)sourceWidth);
				sourcePixelY = (int)(((float)y / (float)destinationHeight) * (float)sourceHeight);
				sourcePixel = interim.pixels[(sourcePixelY * interim.width) + sourcePixelX];

				if (alphaCheck(sourcePixel) ^ inverted)
				{
					this.pixels[((destinationY + y) * this.width) + (destinationX + x)] = Program.COLOUR;
				}
			}
		}

		return this;
	}


	public Bitmap drawBitmap(Bitmap bitmap, int offsetX, int offsetY, int sourceX, int sourceY, int sourceWidth, int sourceHeight, boolean inverted)
	{
		int targetX = 0;
		int targetY = 0;

		for (int x = sourceX; x < sourceX + sourceWidth; x++)
		{
			for (int y = sourceY; y < sourceY + sourceHeight; y++)
			{
				targetX = offsetX + x - sourceX;
				targetY = offsetY + y - sourceY;

				if (targetX < 0 || targetX >= this.width)
					continue;
				if (targetY < 0 || targetY >= this.height)
					continue;

				if (alphaCheck(bitmap.pixels[(y * bitmap.width) + x]) ^ inverted)
				{
					this.pixels[(targetY * this.width) + targetX] = Program.COLOUR;
				}
			}
		}

		return this;
	}


	public Bitmap drawStringSingle(float cellX, float cellY, boolean inverted, String text)
	{
		for (int index = 0; index < text.length(); index++)
		{
			char character = text.charAt(index);

			int charX = (int)character;
			int charY = 0;
			while (charX >= 16)
			{
				charX -= 16;
				charY++;
			}

			drawBitmapScaled(GraphicsResources.font, (int)(cellX * 16) + (index * 8), (int)(cellY * 24), 8, 24, (charX * 8), (charY * 12), 8, 12, inverted);
		}

		return this;
	}

	public Bitmap drawStringSingle(float cellX, float cellY, boolean inverted, int... characters)
	{
		char[] converted = new char[characters.length];
		for (int i = 0; i < converted.length; i++)
		{
			converted[i] = (char)characters[i];
		}
		return drawStringSingle(cellX, cellY, inverted, new String(converted));
	}

	public Bitmap drawStringDouble(float cellX, float cellY, boolean inverted, String text)
	{
		for (int index = 0; index < text.length(); index++)
		{
			char character = text.charAt(index);

			int charX = (int)character;
			int charY = 0;
			while (charX >= 16)
			{
				charX -= 16;
				charY++;
			}

			drawBitmapScaled(GraphicsResources.font, (int)(cellX * 16) + (index * 16), (int)(cellY * 24), 16, 24, (charX * 8), (charY * 12), 8, 12, inverted);
		}

		return this;
	}

	public Bitmap drawStringDouble(float cellX, float cellY, boolean inverted, int... characters)
	{
		char[] converted = new char[characters.length];
		for (int i = 0; i < converted.length; i++)
		{
			converted[i] = (char)characters[i];
		}
		return drawStringDouble(cellX, cellY, inverted, new String(converted));
	}

}
