package net.jaffa.temus.graphics;

import net.jaffa.temus.Program;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;


public class GraphicsResources {
    public static Bitmap font;
    public static BufferedImage dirt;
    public static BufferedImage mask;
//    public static BufferedImage border;


    public static void loadResources() {
        BufferedImage input;

        try {
            input = ImageIO.read(Program.class.getResource("/png/font.png"));
            font = new Bitmap(input.getWidth(), input.getHeight());
            font.pixels = input.getRGB(0, 0, input.getWidth(), input.getHeight(), null, 0, input.getWidth());

            mask = ImageIO.read(Program.class.getResource("/png/mask.png"));
            dirt = ImageIO.read(Program.class.getResource("/png/dirty.png"));
//            border = ImageIO.read(Program.class.getResource("/png/border.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
