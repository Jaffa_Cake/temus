package net.jaffa.temus;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;

import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.graphics.GraphicsResources;
import net.jaffa.temus.menu.*;
import net.jaffa.temus.sfx.SoundPlayer;
import net.jaffa.temus.train.ACarriage;
import net.jaffa.temus.train.CarriageD;
import net.jaffa.temus.train.CarriageN;



public class Program extends Canvas implements Runnable
{
	private static final long serialVersionUID = -5410736293924665688L;
	public static Random random = new Random();

	public static final int WIDTH = 640;
	public static final int HEIGHT = 384;
	public static final int SCALE = 1;
	public static final int BUFFER = 10;
	public static int COLOUR = 0xFFE3BF18;

	public static JFrame window;
	public static Program program;

	public int redraw = 0;
	public int warmth = 0;
	public final int warmthTarget = 540;
	public boolean butterflyKey = false;
	public boolean keyDown = false;
	public boolean fingerDown = false;
	public int fingerX = 0;
	public int fingerY = 0;
	public int depth = 0;
	public int fingerCooldown = 0;
	public int ticks;
	private int buzzTicks = 0;
	private boolean running = false;

	public int odometer = (int)(random.nextFloat() * 2000000);
	public LocalTime timeNow;
	public LocalDate dateNow;

	public Bitmap screen;
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
	private int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	private int[] deadLines;


	public boolean radioFocused = false;
	public String runNumber = "    ";
	public String destination = "  ";
	public MenuTrainRadio radio;
	public AMenu menu;
	public ArrayList<ACarriage> train;


	public Program()
	{
		setPreferredSize(new Dimension((WIDTH * SCALE) + (2 * BUFFER), (HEIGHT * SCALE) + (2 * BUFFER)));
		setBackground(Color.BLACK);

		addMouseListener(new MouseListener()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				;
			}

			@Override
			public void mousePressed(MouseEvent e)
			{
				if (warmth < warmthTarget)
				{
					return;
				}

				if (fingerCooldown > 0)
				{
					return;
				}

				fingerX = (int)Math.floor((e.getX() - BUFFER) / (16.0F * SCALE));
				fingerY = (int)Math.floor((e.getY() - BUFFER) / (24.0F * SCALE));
				fingerDown = true;
			}

			@Override
			public void mouseReleased(MouseEvent e)
			{
				fingerDown = false;
			}

			@Override
			public void mouseEntered(MouseEvent e)
			{
				;
			}

			@Override
			public void mouseExited(MouseEvent e)
			{
				;
			}
		});

		addKeyListener(new KeyListener()
		{
			@Override
			public void keyTyped(KeyEvent e)
			{
				;
			}

			@Override
			public void keyPressed(KeyEvent e)
			{
				if (warmth < warmthTarget)
				{
					return;
				}

				if (keyDown)
				{
					return;
				}

				keyDown = true;

				switch (e.getKeyCode())
				{
					case KeyEvent.VK_NUMPAD1:
					{
						menu = new MenuMenuOptions();
						radioFocused = false;
						redraw();
						SoundPlayer.playBeep();
						break;
					}
					case KeyEvent.VK_NUMPAD4:
					{
						radioFocused = !radioFocused;
						if (menu != null)
						{
							menu.age = 0;
						}
						radio.age = 0;
						redraw();
						SoundPlayer.playBeep();
						break;
					}
					case KeyEvent.VK_NUMPAD7:
					{
						butterflyKey = !butterflyKey;
						if (butterflyKey)
						{
							menu = new MenuRunNumber(true);
						}
						redraw();
						break;
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				keyDown = false;
			}
		});

		timeNow = LocalTime.now();
		dateNow = LocalDate.now();

		screen = new Bitmap(WIDTH, HEIGHT);

		radio = new MenuTrainRadio();
		menu = new MenuRunNumber(true);

		train = new ArrayList<ACarriage>();
		train.add(new CarriageD("6200", true, true));
		train.add(new CarriageN("5200", false));
		train.add(new CarriageN("5199", true));
		train.add(new CarriageD("6199", false, false));
		train.add(new CarriageD("6150", true, false));
		train.add(new CarriageN("5150", false));
		train.add(new CarriageN("5149", true));
		train.add(new CarriageD("6149", false, false));

		((CarriageD)train.get(3)).compressor = false;

		train.get(0).faults.add(21);
		train.get(1).faults.add(21);
		train.get(2).faults.add(21);
		train.get(3).faults.add(21);
		train.get(3).faults.add(88);
		train.get(4).faults.add(11);
		train.get(4).faults.add(21);
		train.get(4).faults.add(33);
		train.get(4).faults.add(37);
		train.get(4).faults.add(88);
		train.get(5).faults.add(21);
		train.get(6).faults.add(21);
		train.get(7).faults.add(21);
		train.get(7).faults.add(88);

		deadLines = new int[random.nextInt(5)];
		for (int i = 0; i < deadLines.length; i++)
		{
			deadLines[i] = random.nextInt(HEIGHT);
		}

		SoundPlayer.playBackground();
	}


	public static void main(String[] args)
	{
		window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);

		window.add(program = new Program());
		window.pack();

		window.setLocationRelativeTo(null);
		window.setVisible(true);

		GraphicsResources.loadResources();

		program.start();
	}


	public void redraw()
	{
		for (int i = 0; i < pixels.length; i++)
		{
			pixels[i] = 0xFF000000;
		}

		redraw = 0;
	}


	public void render()
	{
		if (warmth < warmthTarget)
		{
			renderStartup(screen, warmth);
		}
		else
		{
			if (!butterflyKey)
			{
				Bitmap topBar = new Bitmap(WIDTH, 1 * 24);
				topBar.drawStringDouble(35.5F + (timeNow.getHour() >= 10 ? -1.0F : 0.0F), 0.0F, false, timeNow.getHour() + (ticks >= 30 ? ":" : " ") + String.format("%02d", timeNow.getMinute()));

				screen.clear(0xFF000000);
				screen.drawBitmap(topBar, 0, 0, false);
			}
			else
				/*
				 * Screen Rendering
				 */
			{
				if (menu != null && menu.isFullscreen())
				{
					screen.clear(0xFF000000);
					menu.renderFull(screen);
				}
				else
				{
					Bitmap topBar = new Bitmap(WIDTH, 1 * 24);
					Bitmap topLevel = new Bitmap(WIDTH, (4 * 24) - 1);
					Bitmap bottomLevel = new Bitmap(WIDTH, HEIGHT - (5 * 24));

					topBar.drawStringSingle(0.0F, 0.0F, false, "Run No.");
					topBar.drawStringDouble(5.0F, 0.0F, false, runNumber);
					topBar.drawStringDouble(35.5F + (timeNow.getHour() >= 10 ? -1.0F : 0.0F), 0.0F, false, timeNow.getHour() + (ticks >= 30 ? ":" : " ") + String.format("%02d", timeNow.getMinute()));


					if (radioFocused)
					{
						radio.renderFull(bottomLevel);

						if (menu != null)
						{
							menu.renderMinimised(topLevel);
						}
					}
					else
					{
						radio.renderMinimised(topLevel);

						if (menu != null)
						{
							menu.renderFull(bottomLevel);
						}
					}

					for (int i = 0; i < screen.width * 24; i++)
					{
						screen.pixels[i] = 0xFF000000;
					}

					if ((!radioFocused && radio.age != 0) || (radioFocused && (menu == null || menu.age != 0)))
					{
						for (int i = screen.width * 24; i < screen.width * (5 * 24); i++)
						{
							screen.pixels[i] = 0xFF000000;
						}
					}

					if ((radioFocused && radio.age != 0) || (!radioFocused && (menu == null || menu.age != 0)))
					{
						for (int i = screen.width * (5 * 24); i < screen.width * screen.height; i++)
						{
							screen.pixels[i] = 0xFF000000;
						}
					}

					screen.drawBitmap(topBar, 0, 0, false);
					if (radioFocused && menu != null && !(menu instanceof MenuBlank) && menu.age != 0)
					{
						screen.drawBitmap(topLevel, 0, 1 * 24, true);
					}
					else if (radio.age != 0)
					{
						screen.drawBitmap(topLevel, 0, 1 * 24, false);
					}
					screen.drawBitmap(bottomLevel, 0, 5 * 24, false);

					// Line
					int y = (5 * 24) - 2;
					for (int x = 0; x < WIDTH; x++)
					{
						screen.pixels[(y * WIDTH) + x] = 0xFFFFFFFF;
						screen.pixels[((y + 1) * WIDTH) + x] = 0xFFFFFFFF;
					}
				}
			}
		}

		/*
		 * Post Processing
		 */
		{
			// Jitter
			if (!(menu != null && !radioFocused && !menu.isFinishedRendering()))
			{
				int max = random.nextFloat() > 0.99F ? 500 : 5;
				for (int i = 0; i < max; i++)
				{
					int y = (int)Math.floor(random.nextFloat() * HEIGHT);

					if (random.nextFloat() >= 0.50F)
					{
						for (int x = 0; x < WIDTH - 1; x++)
						{
							screen.pixels[(y * WIDTH) + x] = screen.pixels[(y * WIDTH) + x + 1];
						}
					}
					else
					{
						for (int x = WIDTH - 1; x > 1; x--)
						{
							screen.pixels[(y * WIDTH) + x] = screen.pixels[(y * WIDTH) + x - 1];
						}
					}
				}
			}

			// Dead lines
			for (int i = 0; i < deadLines.length; i++)
			{
				int y = deadLines[i];
				for (int x = 0; x < WIDTH; x++)
				{
					screen.pixels[(y * WIDTH) + x] = 0xFF000000;
				}
			}

			// Pixels
			for (int i = 0; i < screen.pixels.length; i++)
			{
				if ((screen.pixels[i] & 0x00FFFFFF) != 0)
				{
					screen.pixels[i] = COLOUR;
				}
			}

			// Redraw
			if (redraw < pixels.length)
			{
				redraw += WIDTH * 7;
			}
		}

		/*
		 * Java2D Rendering
		 */
		{
			BufferStrategy bs = getBufferStrategy();
			{
				Graphics g;

				if (bs == null)
				{
					createBufferStrategy(2);
					return;
				}

				if (redraw < WIDTH)
				{
					for (int x = 0; x < pixels.length; x++)
					{
						pixels[x] = 0xFF000000;
					}
				}
				else
				{
					for (int x = 0; x < pixels.length; x++)
					{
						//						if (x > redraw)
						//						{
						//							break;
						//						}

						pixels[x] = screen.pixels[x];
					}
				}

				g = bs.getDrawGraphics();
				{
					g.clearRect(0, 0, getWidth(), getHeight());
					g.drawImage(image, BUFFER, BUFFER, getWidth() - (2 * BUFFER), getHeight() - (2 * BUFFER), null);
					g.drawImage(GraphicsResources.mask, BUFFER, BUFFER, getWidth() - (2 * BUFFER), getHeight() - (2 * BUFFER), null);
					g.drawImage(GraphicsResources.dirt, 0, 0, getWidth(), getHeight(), null);
					//                    g.drawImage(GraphicsResources.border, -150, 0, 1700, 890, null);
				}
				g.dispose();
			}
			bs.show();
		}
	}


	private void renderStartup(Bitmap target, int warmth)
	{
		target.clear(0xFF000000);

		if (warmth >= 80 && warmth < warmthTarget - 14)
		{
			target.drawStringSingle(18.0F, 6.0F, false, 0x98, 0x99, 0x9A);
			target.drawStringSingle(17.5F, 7.0F, false, 0xA8, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xAA);
			target.drawStringSingle(21.5F, 8.0F, false, 0xB8, 0xB9, 0xBA);

			target.drawStringDouble(14.0F, 9.0F, false, "T A N G A R A");

			if (warmth >= 82)
			{
				target.drawStringDouble(9.0F, 11.0F, false, "TRAIN MANAGEMENT SYSTEM");
			}

			if (warmth >= 84)
			{
				target.drawStringDouble(7.0F, 13.0F, false, "WARMING UP--PLEASE STAND-BY");
			}

			if (warmth >= 86)
			{
				target.drawStringSingle(7.0F, 15.0F, false, "Evaluation Version 2.1");
				target.drawStringDouble(18.0F, 15.0F, false, "B");
			}
		}
	}


	@Override
	public void run()
	{
		final double nsPerTick = 1000000000.0D / 60.0D;
		long before = System.nanoTime();
		long now;
		double unprocessedTicks = 0.0D;
		ticks = 0;
		int frames = 0;
		long logTimer = System.currentTimeMillis();

		while (running)
		{
			now = System.nanoTime();
			unprocessedTicks += (now - before) / nsPerTick;
			before = now;

			while (unprocessedTicks >= 1.0D)
			{
				tick();
				ticks++;
				unprocessedTicks -= 1.0D;
			}

			try
			{
				Thread.sleep(2L);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			finally
			{
				render();
				frames++;
			}

			if (System.currentTimeMillis() - logTimer >= 1000)
			{
				logTimer += 1000;

				//				System.out.println(ticks + " TPS, " + frames + " FPS");

				ticks = 0;
				frames = 0;
			}
		}
	}


	public void start()
	{
		running = true;

		new Thread(this).run();
	}

	public void stop()
	{
		running = false;
	}


	public void tick()
	{
		timeNow = LocalTime.now();
		dateNow = LocalDate.now();

		if (warmth < warmthTarget)
		{
			warmth++;

			if (warmth == warmthTarget || warmth < 80)
			{
				redraw();
			}
		}

		if (buzzTicks < 1046)
		{
			buzzTicks++;

			if (buzzTicks == 1046)
			{
				SoundPlayer.playBuzz();
			}
		}

		if (menu == null && butterflyKey)
		{
			menu = new MenuBlank();
		}
		else if (menu instanceof ITickable)
		{
			((ITickable)menu).tick();
		}


		/*
		 * Ensure that the current menu (if there is one) and the train radio don't render at the same time.
		 * Give rendering priority to whatever is on the bottom half of the TMS.
		 */
		if (radioFocused)
		{
			radio.age++;

			if (radio.isFinishedRendering() && menu != null)
			{
				menu.age++;
			}
		}
		else
		{
			if (menu != null)
			{
				menu.age++;

				if (menu.isFinishedRendering())
				{
					radio.age++;
				}
			}
			else
			{
				radio.age++;
			}
		}


		if (!butterflyKey)
		{
			menu = null;
		}

		if (fingerDown)
		{
			fingerCooldown = 20;

			depth++;

			if (depth == 5 || (depth >= 30 && depth % 40 == 0))
			{
				boolean beep = false;

				if (radioFocused)
				{
					beep = radio.activateSoft(fingerX, fingerY);
				}
				else
				{
					if (menu != null)
					{
						beep = menu.activateSoft(fingerX, fingerY);
					}
				}

				if (beep)
				{
					SoundPlayer.playBeep();
				}
			}

			if (depth == 150)
			{
				depth = -50;
				boolean beep = false;

				if (radioFocused)
				{
					beep = radio.activateHard(fingerX, fingerY);
				}
				else
				{
					if (menu != null)
					{
						beep = menu.activateHard(fingerX, fingerY);
					}
				}

				if (beep)
				{
					SoundPlayer.playBeep();
				}
			}
		}
		else
		{
			depth = 0;

			if (fingerCooldown > 0)
			{
				fingerCooldown--;
			}
		}
	}
}
