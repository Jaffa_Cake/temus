package net.jaffa.temus.menu;

import net.jaffa.temus.graphics.Bitmap;



public class MenuBlank extends AMenu
{
	@Override
	public boolean activateSoft(int x, int y)
	{
		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 2;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");
	}
}
