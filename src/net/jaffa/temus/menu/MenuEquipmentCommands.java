package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.train.ACarriage;
import net.jaffa.temus.train.CarriageD;
import net.jaffa.temus.train.CarriageN;



public class MenuEquipmentCommands extends AMenu
{
	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return true;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "CUTOUT/IN COMMANDS");
		target.drawStringSingle(21.0F, 0.0F, false, 0x82, 0x83);

		target.drawStringDouble(2.0F, 1.0F, false, "CAR");
		target.drawStringDouble(2.0F, 2.0F, false, "CH-M1");
		target.drawStringDouble(2.0F, 4.0F, false, "CH-M2");
		target.drawStringDouble(2.0F, 6.0F, false, "CH-");
		target.drawStringSingle(5.0F, 6.0F, false, "M1&2");
		target.drawStringDouble(2.0F, 7.0F, false, "SIV");

		// Train
		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, false);
		}

		// Options
		ACarriage car;
		for (int i = 0; i < Program.program.train.size(); i++)
		{
			car = Program.program.train.get(i);

			if (car instanceof CarriageD)
			{
				target.drawStringSingle(8.0F + (i * 4.0F), 7.0F, false, " IN ");
				drawButtonBorder(target, 8 + (i * 4), 7, 2, true, false);
			}
			else if (car instanceof CarriageN)
			{
				drawButtonBorder(target, 7.5F + (i * 4.0F), 6, 3, true, true);

				target.drawStringSingle(8.0F + (i * 4.0F) + (car.reversed ? -0.5F : 0.5F), 4.0F, false, " IN ");
				drawButtonBorder(target, 8 + (i * 4) + (car.reversed ? -0.5F : 0.5F), 4, 2, true, false);

				target.drawStringSingle(8.0F + (i * 4.0F) + (car.reversed ? 0.5F : -0.5F), 2.0F, true, " IN ");
				drawButtonBorderNoTop(target, 8 + (i * 4) + (car.reversed ? 0.5F : -0.5F), 2, 2);
			}
		}

		target.drawStringDouble(1.0F, 9.0F, false, "CLR");

		drawButtonBorder(target, 1, 9, 3);

		target.drawStringDouble(36.0F, 9.0F, false, "ACK");

		drawButtonBorder(target, 36, 9, 3);

	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "CUTOUT/IN COMMANDS");
		target.drawStringSingle(24.0F, 1.0F, false, 0x82, 0x83);
	}
}
