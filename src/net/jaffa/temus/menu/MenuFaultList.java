package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.train.ACarriage;
import net.jaffa.temus.train.Faults;



public class MenuFaultList extends AMenu
{
	private int carIndex = -1;

	public boolean startup;

	public MenuFaultList()
	{
		this(false);
	}

	public MenuFaultList(boolean startup)
	{
		this.startup = startup;
	}

	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					if (startup)
					{
						Program.program.menu = new MenuDestination();
					}
					else
					{
						Program.program.menu = new MenuBlank();
					}
					Program.program.redraw();
					return true;
				}

				case 36:
				case 37:
				case 38:
				{
					while (true)
					{
						carIndex++;

						if (carIndex >= Program.program.train.size())
						{
							carIndex = -1;
							age = 0;
							Program.program.redraw();
							break;
						}

						if (Program.program.train.get(carIndex).faults.size() != 0)
						{
							age = 16;
							Program.program.redraw();
							Program.program.redraw = Program.WIDTH * (Program.HEIGHT / 3);
							break;
						}
					}

					return true;
				}
			}
		}
		else if (y == 6)
		{
			if (x >= 7 && x <= 7 + (Program.program.train.size() * 4))
			{
				carIndex = (int)Math.floor((x - 7.0D) / 4.0D);
				Program.program.redraw();
				Program.program.redraw = Program.WIDTH * (Program.HEIGHT / 3);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 16 + Program.program.train.size();
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "FAULT CHECK INFORMATION");
		target.drawStringSingle(26.0F, 0.0F, false, 0x82, 0x83);

		target.drawStringDouble(2.0F, 1.0F, false, "CAR");

		if (age < 2)
			return;

		if (carIndex < 0)
		{
			target.drawStringSingle(5.0F, 8.0F, false, "[Symbol]");
			target.drawStringSingle(11.0F, 8.0F, false, 0x84, 0x85);
			target.drawStringDouble(12.0F, 8.0F, false, ";");
			target.drawStringSingle(13.0F, 8.0F, false, "NORMAL");
			target.drawStringSingle(18.0F, 8.0F, false, 0x86, 0x87);
			target.drawStringDouble(19.0F, 8.0F, false, ";");
			target.drawStringSingle(20.0F, 8.0F, false, "ABNORMAL");

			if (age < 4)
				return;

			target.drawStringDouble(26.0F, 8.0F, false, "-;");
			target.drawStringSingle(28.0F, 8.0F, false, "UNKNOWN");
		}

		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringDouble(36.0F, 9.0F, flash, "ACK");
		drawButtonBorder(target, 36, 9, 3);

		if (age < 6)
			return;

		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, carIndex == x);
			if (age < 8 + x)
				return;
		}

		if (carIndex < 0)
		{
			// Circles
			for (int i = 0; i < Program.program.train.size(); i++)
			{
				ACarriage car = Program.program.train.get(i);

				if (car.faults.size() != 0)
				{
					target.drawStringSingle(9.0F + (i * 4.0F), 3.0F, false, 0x86, 0x87);
				}
				else
				{
					target.drawStringSingle(9.0F + (i * 4.0F), 3.0F, false, 0x84, 0x85);
				}

				if (age < 16 + (int)(i * 0.5F))
					return;
			}
		}
		else // age 16+
		{
			ACarriage car = Program.program.train.get(carIndex);

			if (car.faults.size() != 0)
			{
				for (int i = 0; i < car.faults.size(); i++)
				{
					target.drawStringDouble(5.0F, 3.0F + i, false, String.valueOf(car.faults.get(i)));
					target.drawStringDouble(8.0F, 3.0F + i, false, Faults.getFaultText(car.faults.get(i)));

					if (age < 16 + i)
						return;
				}
			}
		}
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "FAULT CHECK INFORMATION");
		target.drawStringSingle(29.0F, 1.0F, false, 0x82, 0x83);
	}
}
