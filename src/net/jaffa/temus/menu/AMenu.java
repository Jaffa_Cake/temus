package net.jaffa.temus.menu;

import net.jaffa.temus.graphics.Bitmap;



public abstract class AMenu
{
	public int age = 0;

	public AMenu()
	{
		age = 0;
	}

	public abstract boolean activateSoft(int x, int y);

	public abstract boolean activateHard(int x, int y);

	protected static void drawButtonBorder(Bitmap target, int x, int y, int width)
	{
		drawButtonBorder(target, (float)x, (float)y, width);
	}

	protected static void drawButtonBorder(Bitmap target, float x, float y, int width)
	{
		drawButtonBorder(target, x, y, width, false, false);
	}

	protected static void drawButtonBorder(Bitmap target, float x, float y, int width, boolean leftTuckedIn, boolean rightTuckedIn)
	{
		for (int i = 0; i < width; i++)
		{
			target.drawStringDouble(x + (float)i, y, false, 0x96);
			target.drawStringDouble(x + (float)i, y - 1.0F, false, 0x96);
		}

		if (leftTuckedIn)
		{
			target.drawStringSingle(x, y, false, 0x97);
		}
		else
		{
			target.drawStringSingle(x - 0.5F, y, false, 0x95);
		}

		if (rightTuckedIn)
		{
			target.drawStringSingle(x + (float)width - 0.5F, y, false, 0x95);
		}
		else
		{
			target.drawStringSingle(x + (float)width, y, false, 0x97);
		}
	}

	protected static void drawButtonBorderNoTop(Bitmap target, float x, float y, int width)
	{
		for (int i = 0; i < width; i++)
		{
			target.drawStringDouble(x + (float)i, y, false, 0x96);
		}
		target.drawStringSingle(x - 0.5F, y, false, 0x95);
		target.drawStringSingle(x + (float)width, y, false, 0x97);
	}

	protected static void drawMenuIndicator(Bitmap target, String name)
	{
		target.drawStringDouble(40.0F - (float)(name.length()), 0.0F, false, name);
		target.drawStringSingle(39.5F - (float)(name.length()), 0.0F, false, 0x95);

		for (int x = 0; x < name.length(); x++)
		{
			target.drawStringDouble(40.0F - (float)(name.length()) + x, 0.0F, false, 0x96);
		}
	}

	public abstract boolean isFinishedRendering();

	public boolean isFullscreen()
	{
		return false;
	}

	public abstract void renderFull(Bitmap target);

	public abstract void renderMinimised(Bitmap target);

}
