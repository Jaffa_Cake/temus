package net.jaffa.temus.menu;

public interface ITickable
{
	void tick();
}
