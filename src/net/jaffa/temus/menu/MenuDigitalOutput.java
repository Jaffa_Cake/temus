package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.train.ACarriage;
import net.jaffa.temus.train.CarriageD;



public class MenuDigitalOutput extends AMenu
{
	int pairIndex = 0;

	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					return true;
				}

				case 19:
				case 20:
				case 21:
				{
					Program.program.menu = new MenuTesting();
					Program.program.redraw();
					return true;
				}

				case 36:
				case 37:
				case 38:
				{
					pairIndex += 2;

					if (pairIndex >= Program.program.train.size())
					{
						pairIndex = 0;
					}

					Program.program.redraw();
					Program.program.redraw = Program.WIDTH * (Program.HEIGHT / 3);

					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return true;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");
		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "DIGITAL OUTPUT CHECK");
		target.drawStringSingle(23.0F, 0.0F, false, 0x82, 0x83);

		// Render Train
		target.drawStringDouble(2.0F, 1.0F, false, "CAR");
		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, x == pairIndex || x == pairIndex + 1);
		}

		// Information
		target.drawStringDouble(2.0F, 2.0F, false, "MCU");
		target.drawStringDouble(2.0F, 5.0F, false, "CA");

		for (int x = 0; x <= 3; x++)
		{
			target.drawStringSingle(7.0F + (9.0F * x), 2.0F, false, "[" + x + "]");
		}

		for (int y = 1; y <= 2; y++)
		{
			target.drawStringSingle(2.0F, 2.0F + (y * 1.0F), false, "(" + y + ")");
			target.drawStringSingle(2.0F, 5.0F + (y * 1.0F), false, "(" + y + ")");
		}

		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				target.drawStringDouble(4.0F + (x * 9.0F), 3.0F + (y * 1.0F), false, "00000000");
			}
		}

		for (int x = 0; x < 4; x++)
		{
			target.drawStringDouble(4.0F + (x * 9.0F), 6.0F, false, "00000000");
		}
		target.drawStringDouble(4.0F, 7.0F, false, "00000000");


		// Render Buttons
		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringSingle(19.5F, 9.0F, false, "MENU");
		drawButtonBorder(target, 19, 9, 3);

		target.drawStringDouble(36.0F, 9.0F, flash, "ACK");
		drawButtonBorder(target, 36, 9, 3);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "DIGITAL OUTPUT CHECK");
		target.drawStringSingle(26.0F, 1.0F, false, 0x82, 0x83);
	}
}
