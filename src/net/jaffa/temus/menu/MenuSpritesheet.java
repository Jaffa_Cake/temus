package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuSpritesheet extends AMenu
{
	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return true;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "SPRITESHEET");
		target.drawStringSingle(14.0F, 0.0F, false, 0x82, 0x83);

		for (int i = 0x00; i <= 0x7F; i++)
		{
			int x = i & 0x0F;
			int y = (i >> 4) & 0x0F;

			target.drawStringDouble(4.0F + (x * 1.0F), 1.0F + (y * 1.0F), false, i);
		}

		for (int i = 0x80; i <= 0xFF; i++)
		{
			int x = i & 0x0F;
			int y = (i >> 4) & 0x0F;

			target.drawStringDouble(20.0F + (x * 1.0F), -7.0F + (y * 1.0F), false, i);
		}

		for (int x = 0; x < 32; x++)
		{
			target.drawStringDouble(4.0F + (x * 1.0F), 0.0F, false, 0x96);
			target.drawStringDouble(4.0F + (x * 1.0F), 8.0F, false, 0x96);
		}
		for (int y = 0; y < 8; y++)
		{
			target.drawStringSingle(3.5F, 1.0F + (y * 1.0F), false, 0x95);
			target.drawStringSingle(20.0F, 1.0F + (y * 1.0F), false, 0x97);
			target.drawStringSingle(36.0F, 1.0F + (y * 1.0F), false, 0x97);
		}

		target.drawStringSingle(10.0F, 9.0F, false, "0x00-0x7F");
		target.drawStringSingle(26.0F, 9.0F, false, "0x80-0xFF");

		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3, false, true);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "SPRITESHEET");
		target.drawStringSingle(17.0F, 1.0F, false, 0x82, 0x83);
	}
}
