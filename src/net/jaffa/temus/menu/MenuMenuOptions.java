package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuMenuOptions extends AMenu
{

	private int selectedOption;

	public MenuMenuOptions()
	{
		selectedOption = 0;
	}

	@Override
	public boolean activateSoft(int x, int y)
	{
		switch (y)
		{
			case 7:
			{
				if (x >= 25 && x <= 27)
				{
					selectedOption = 1;
					return true;
				}
				break;
			}

			case 8:
			{
				if (x >= 30 && x <= 32)
				{
					selectedOption = 2;
					return true;
				}
				break;
			}

			case 9:
			{
				if (x >= 25 && x <= 27)
				{
					selectedOption = 3;
					return true;
				}
				break;
			}

			case 10:
			{
				if (x >= 30 && x <= 32)
				{
					selectedOption = 4;
					return true;
				}
				break;
			}

			case 11:
			{
				if (x >= 25 && x <= 27)
				{
					selectedOption = 5;
					return true;
				}
				break;
			}

			case 14:
			{
				if (x >= 1 && x <= 3)
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}
				else if (x >= 36 && x <= 38)
				{
					switch (selectedOption)
					{
						case 1:
						{
							Program.program.menu = new MenuRunNumber();
							Program.program.redraw();
							return true;
						}

						case 2:
						{
							Program.program.menu = new MenuDestination();
							Program.program.redraw();
							return true;
						}

						case 3:
						{
							Program.program.menu = new MenuEquipmentCommands();
							Program.program.redraw();
							return true;
						}

						case 4:
						{
							Program.program.menu = new MenuFaultList();
							Program.program.redraw();
							return true;
						}

						case 5:
						{
							Program.program.menu = new MenuOtherInformation();
							Program.program.redraw();
							return true;
						}
					}
				}
				break;
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 18;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "MENU OPTIONS");
		target.drawStringSingle(15.0F, 0.0F, false, 0x82, 0x83);

		if (age < 2)
			return;

		target.drawStringDouble(2.0F, 2.0F, false, "1.RUN NO. SETTING ~~~~");
		target.drawStringDouble(25.0F, 2.0F, selectedOption == 1, "R");
		target.drawStringSingle(26.0F, 2.0F, selectedOption == 1, ".NO.");
		drawButtonBorder(target, 25, 2, 3);
		if (age < 5)
			return;

		target.drawStringDouble(2.0F, 3.0F, false, "2.DESTINATION SETTING ~~~~~");
		target.drawStringDouble(30.0F, 3.0F, selectedOption == 2, "DES");
		drawButtonBorder(target, 30, 3, 3);
		if (age < 8)
			return;

		target.drawStringDouble(2.0F, 4.0F, false, "3.EQUIPMENT CUTOUT ~~~");
		target.drawStringDouble(25.0F, 4.0F, selectedOption == 3, "CUT");
		drawButtonBorder(target, 25, 4, 3);
		if (age < 11)
			return;

		target.drawStringDouble(2.0F, 5.0F, false, "4.FAULT CHECK INFORMATION ~");
		target.drawStringDouble(30.0F, 5.0F, selectedOption == 4, "FLT");
		drawButtonBorder(target, 30, 5, 3);
		if (age < 14)
			return;

		target.drawStringDouble(2.0F, 6.0F, false, "5.OTHER INFORMATION ~~");
		target.drawStringDouble(25.0F, 6.0F, selectedOption == 5, "O");
		target.drawStringSingle(26.0F, 6.0F, selectedOption == 5, "ther");
		drawButtonBorder(target, 25, 6, 3);
		if (age < 17)
			return;

		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringDouble(36.0F, 9.0F, selectedOption != 0 && flash, "ACK");
		drawButtonBorder(target, 36, 9, 3);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "MENU OPTIONS");
		target.drawStringSingle(18.0F, 1.0F, false, 0x82, 0x83);
	}
}
