package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuDestination extends AMenu implements ITickable
{
	private int index;

	private StringBuilder input;
	private String[] interimDestination = getDestination("99");


	public MenuDestination()
	{
		index = 1;
		input = new StringBuilder(2);
		input.append("  ");
	}

	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 8 && index != 0)
		{
			switch (x)
			{
				case 2:
				case 3:
				case 4:
					input.setCharAt(index - 1, '0');
					index++;
					return true;
				case 5:
				case 6:
				case 7:
					input.setCharAt(index - 1, '1');
					index++;
					return true;
				case 8:
				case 9:
				case 10:
					input.setCharAt(index - 1, '2');
					index++;
					return true;
				case 11:
				case 12:
				case 13:
					input.setCharAt(index - 1, '3');
					index++;
					return true;
				case 14:
				case 15:
				case 16:
					input.setCharAt(index - 1, '4');
					index++;
					return true;
				case 17:
				case 18:
				case 19:
					input.setCharAt(index - 1, '5');
					index++;
					return true;
				case 20:
				case 21:
				case 22:
					input.setCharAt(index - 1, '6');
					index++;
					return true;
				case 23:
				case 24:
				case 25:
					input.setCharAt(index - 1, '7');
					index++;
					return true;
				case 26:
				case 27:
				case 28:
					input.setCharAt(index - 1, '8');
					index++;
					return true;
				case 29:
				case 30:
				case 31:
					input.setCharAt(index - 1, '9');
					index++;
					return true;
			}
		}
		else if (y == 10)
		{
			switch (x)
			{
				case 33:
				case 34:
				case 35:
				{
					index--;
					return true;
				}

				case 37:
				case 38:
				case 39:
				{
					index++;
					return true;
				}
			}
		}
		else if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;

				case 36:
				case 37:
				case 38:
					if (index == 0)
					{
						String[] potentiallyNewDestination = getDestination(input.toString());

						if (interimDestination[0] == potentiallyNewDestination[0] && interimDestination[1] == potentiallyNewDestination[1])
						{
							Program.program.destination = input.toString();
							Program.program.menu = new MenuBlank();
						}
						else
						{
							interimDestination = getDestination(input.toString());
						}
						Program.program.redraw();
						return true;
					}
					break;
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 22;
	}

	public static String[] getDestination(String code)
	{
		switch (code)
		{
			case "00":
				return new String[] { "  CITY CIRCLE   ", "  VIA WYNYARD   " };
			case "01":
				return new String[] { "  CITY CIRCLE   ", "   VIA MUSEUM   " };
			case "02":
				return new String[] { "    ASHFIELD    ", "                " };
			case "03":
				return new String[] { "   BANKSTOWN    ", "  VIA SYDENHAM  " };
			case "04":
				return new String[] { "   BANKSTOWN    ", "  VIA LIDCOMBE  " };
			case "05":
				return new String[] { "   BANKSTOWN    ", "   VIA SEFTON   " };
			case "06":
				return new String[] { "   BANKSTOWN    ", "   VIA AUBURN   " };
			case "07":
				return new String[] { "    BEROWRA     ", "VIA STRATHFIELD " };
			case "08":
				return new String[] { "    BEROWRA     ", "   VIA SHORE    " };
			case "09":
				return new String[] { "   BLACKTOWN    ", "                " };
			case "10":
				return new String[] { " BONDI JUNCTION ", "                " };
			case "11":
				return new String[] { "  CAMPBELLTOWN  ", "VIA REGENTS PARK" };
			case "12":
				return new String[] { "  CAMPBELLTOWN  ", " VIA GRANVILLE  " };
			case "13":
				return new String[] { "  CAMPBELLTOWN  ", " VIA BANKSTOWN  " };
			case "14":
				return new String[] { "  CAMPBELLTOWN  ", " VIA EAST HILLS " };
			case "15":
				return new String[] { "   CANTERBURY   ", "  VIA SYDENHAM  " };
			case "16":
				return new String[] { "   CANTERBURY   ", "  VIA LIDCOMBE  " };
			case "17":
				return new String[] { "  CARLINGFORD   ", "                " };
			case "18":
				return new String[] { "    CENTRAL     ", "                " };
			case "19":
				return new String[] { "   CHATSWOOD    ", "                " };
			case "20":
				return new String[] { "    CHULLORA    ", "   WORKSHOPS    " };
			case "79":
				return new String[] { "    TANGARA     ", "                " };
			case "99":
			default:
				return new String[] { "                ", "                " };
		}
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "DESTINATION");
		target.drawStringSingle(14.0F, 0.0F, false, 0x82, 0x83);

		target.drawStringSingle(5.0F, 1.0F, false, "DES. CODE");

		for (int i = 1; i <= 2; i++)
		{
			if (index == i && flash)
			{
				target.drawStringDouble(10.0F + i, 1.0F, false, input.charAt(i - 1));
			}
			else
			{
				target.drawStringDouble(10.0F + i, 1.0F, true, input.charAt(i - 1));
			}
		}

		for (int x = 0; x <= 9; x++)
		{
			target.drawStringDouble(3.0F + (3.0F * x), 3.0F, false, (char)('0') + x);
			target.drawStringDouble(3.0F + (3.0F * x), 5.0F, false, (char)('A') + x);
		}
		for (int x = 0; x <= 7; x++)
		{
			target.drawStringDouble(6.0F + (3.0F * x), 7.0F, false, (char)('K') + x);
			target.drawStringDouble(6.0F + (3.0F * x), 9.0F, false, (char)('S') + x);
		}

		for (int x = 0; x <= 9; x++)
		{
			target.drawStringSingle(2.0F + (x * 3.0F), 2.0F, false, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xB5);
			target.drawStringSingle(4.5F + (x * 3.0F), 3.0F, false, 0xC3);
			target.drawStringSingle(2.0F + (x * 3.0F), 4.0F, false, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xC5);
			target.drawStringSingle(4.5F + (x * 3.0F), 5.0F, false, 0xC3);

			if (x <= 8)
			{
				target.drawStringSingle(2.0F + (x * 3.0F), 6.0F, false, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xC5);
			}
			else
			{
				target.drawStringSingle(2.0F + (x * 3.0F), 6.0F, false, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xD5);
			}
		}
		for (int x = 0; x < 8; x++)
		{
			target.drawStringSingle(7.5F + (x * 3.0F), 7.0F, false, 0xC3);
			target.drawStringSingle(5.0F + (x * 3.0F), 8.0F, false, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xC5);
			target.drawStringSingle(7.5F + (x * 3.0F), 9.0F, false, 0xC3);
			target.drawStringSingle(5.0F + (x * 3.0F), 10.0F, false, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xD5);
		}
		target.drawStringSingle(1.5F, 2.0F, false, 0xB3);
		target.drawStringSingle(1.5F, 3.0F, false, 0xC3);
		target.drawStringSingle(1.5F, 4.0F, false, 0xC3);
		target.drawStringSingle(1.5F, 5.0F, false, 0xC3);
		target.drawStringSingle(1.5F, 6.0F, false, 0xD3);
		target.drawStringSingle(4.5F, 7.0F, false, 0xC3);
		target.drawStringSingle(4.5F, 8.0F, false, 0xC3);
		target.drawStringSingle(4.5F, 9.0F, false, 0xC3);
		target.drawStringSingle(4.5F, 10.0F, false, 0xD3);


		for (int i = 0; i <= 1; i++)
		{
			target.drawStringSingle(32.5F + (i * 4.0F), 4.0F, false, 0xB3, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xB5);
			target.drawStringSingle(32.5F + (i * 4.0F), 5.0F, false, 0xC3);
			target.drawStringSingle(35.5F + (i * 4.0F), 5.0F, false, 0xC3);
			target.drawStringSingle(32.5F + (i * 4.0F), 6.0F, false, 0xD3, 0xB6, 0xB6, 0xB6, 0xB6, 0xB6, 0xD5);
		}
		target.drawStringSingle(34.0F, 5.0F, false, 0x88, 0x89);
		target.drawStringSingle(38.0F, 5.0F, false, 0x8E, 0x8F);


		//        for (int x = 0; x < 16; x++) {
		for (int y = 0; y < 2; y++)
		{
			target.drawStringDouble(17.0F, 0.0F + (float)y, true, interimDestination[y]);
		}
		//        }


		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringDouble(36.0F, 9.0F, index == 0 && flash, "ACK");
		drawButtonBorder(target, 36, 9, 3);

		for (int y = (int)(age * 0.5F); y < 11; y += 1)
		{
			for (float x = 0.0F; x < 40.0F; x += 1.0F)
			{
				for (int px = (int)(x * 16); px < (x * 16) + 16; px++)
				{
					for (int py = (y * 24); py < (y * 24) + 24; py++)
					{
						target.pixels[(py * target.width) + px] = 0xFF000000;
					}
				}
			}
		}
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "DESTINATION");
		target.drawStringSingle(17.0F, 1.0F, false, 0x82, 0x83);
	}

	@Override
	public void tick()
	{
		if (index <= -1)
		{
			index = 2;
		}
		else if (index >= 3)
		{
			index = 0;
		}
	}
}
