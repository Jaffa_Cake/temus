package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuDisplayTest extends AMenu implements ITickable
{

	private int page = 0;
	private int variation = 0;
	private int counter = 0;


	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}

				case 19:
				case 20:
				case 21:
				{
					Program.program.menu = new MenuTesting();
					Program.program.radio.age = 0;
					Program.program.redraw();
					return true;
				}

				case 36:
				case 37:
				case 38:
				{
					page++;

					if (page >= 3)
					{
						page = 0;
					}

					variation = 0;
					counter = 0;

					age = 6;

					if (page != 2)
					{
						Program.program.redraw();
					}
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return true;
	}

	private boolean buttonMask(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				case 19:
				case 20:
				case 21:
				case 36:
				case 37:
				case 38:
					return true;
			}
		}

		return false;
	}

	@Override
	public boolean isFullscreen()
	{
		return age >= 6;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		if (age < 6)
		{
			drawMenuIndicator(target, "TMS");

			target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
			target.drawStringDouble(2.0F, 0.0F, false, "DISPLAY CHECK");
			target.drawStringSingle(16.0F, 0.0F, false, 0x82, 0x83);

			if (age < 2)
				return;

			target.drawStringDouble(1.0F, 14.0F, false, "CLR");
			drawButtonBorder(target, 1, 14, 3);

			target.drawStringSingle(19.5F, 14.0F, false, "MENU");
			drawButtonBorder(target, 19, 14, 3);

			target.drawStringDouble(36.0F, 14.0F, flash, "ACK");
			drawButtonBorder(target, 36, 14, 3);

			return;
		}

		switch (page)
		{
			case 0:
			{
				for (int y = 0; y < 16; y++)
				{
					for (int x = 0; x < 40; x++)
					{
						if ((y - x) % 4 == 0)
						{
							if (buttonMask(x, y))
							{
								continue;
							}

							target.drawStringDouble((float)x, (float)y, true, " ");
						}
					}

					if ((age - 6) < (int)(y * 0.75F))
						return;
				}

				break;
			}

			case 1:
			{
				for (int y = 0; y < 16; y++)
				{
					for (int x = 0; x < 40; x++)
					{
						int px = (int)(x / 4);
						int py = (int)(y / 2) + variation;

						if ((px + py) % 2 == 0)
						{
							if (buttonMask(x, y))
							{
								continue;
							}

							target.drawStringDouble((float)x, (float)y, true, " ");
						}
					}

					if ((age - 6) < (int)(y * 0.75F))
						return;
				}
				break;
			}

			case 2:
			{
				for (int y = 0; y < 16; y++)
				{
					for (int x = 0; x < 40; x++)
					{
						if (variation == 0)
						{
							if (buttonMask(x, y))
							{
								continue;
							}
							target.drawStringDouble((float)x, (float)y, true, " ");
						}
					}
				}
				break;
			}
		}

		target.drawStringDouble(1.0F, 14.0F, false, "CLR");
		drawButtonBorder(target, 1, 14, 3);

		target.drawStringSingle(19.5F, 14.0F, false, "MENU");
		drawButtonBorder(target, 19, 14, 3);

		target.drawStringDouble(36.0F, 14.0F, flash, "ACK");
		drawButtonBorder(target, 36, 14, 3);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{

	}

	@Override
	public void tick()
	{
		counter++;

		if (counter >= 180)
		{
			counter = 0;

			variation++;
			age = 6;

			if (page != 2)
			{
				Program.program.redraw();
			}
		}

		if (variation >= 2)
		{
			variation = 0;
		}
	}
}
