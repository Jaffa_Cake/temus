package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuTesting extends AMenu
{

	private int selectedOption;

	public MenuTesting()
	{
		selectedOption = 0;
	}

	@Override
	public boolean activateSoft(int x, int y)
	{
		switch (x)
		{
			case 1:
			case 2:
			case 3:
			{
				if (y == 14) // CLR
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}
				break;
			}

			case 19:
			case 20:
			case 21:
			{
				if (y == 14) // MENU
				{
					Program.program.menu = new MenuOtherInformation();
					Program.program.redraw();
					return true;
				}
				break;
			}

			case 28:
			case 29:
			case 30:
			{
				switch (y) // 1, 3, 5, 7
				{
					case 6:
						selectedOption = 1;
						return true;

					case 8:
						selectedOption = 3;
						return true;

					case 10:
						selectedOption = 5;
						return true;

					case 12:
						selectedOption = 7;
						return true;
				}
				break;
			}

			case 32:
			case 33:
			case 34:
			{
				switch (y) // 2, 4, 6, 8
				{
					case 7:
						selectedOption = 2;
						return true;

					case 9:
						selectedOption = 4;
						return true;

					case 11:
						selectedOption = 6;
						return true;

					case 13:
						selectedOption = 8;
						return true;
				}
				break;
			}

			case 36:
			case 37:
			case 38:
			{
				if (y == 14) // ACK
				{
					switch (selectedOption)
					{
						case 1:
						{
							Program.program.menu = new MenuDisplayTest();
							Program.program.redraw();
							return true;
						}

						case 2:
						{
							Program.program.menu = new MenuCPUTransmission();
							Program.program.redraw();
							return true;
						}

						case 3:
						{
							Program.program.menu = new MenuRamCheck();
							Program.program.redraw();
							return true;
						}

						case 4:
						{
							Program.program.menu = new MenuRomCheck();
							Program.program.redraw();
							return true;
						}

						case 5:
						{
							Program.program.menu = new MenuDigitalInput();
							Program.program.redraw();
							return true;
						}

						case 6:
						{
							Program.program.menu = new MenuDigitalOutput();
							Program.program.redraw();
							return true;
						}

						case 7:
						{
							Program.program.menu = new MenuTestRun();
							Program.program.redraw();
							return true;
						}
					}
				}
				break;
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 28;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "MENU FOR FIRST LEVEL TESTING");
		target.drawStringSingle(31.0F, 0.0F, false, 0x82, 0x83);

		if (age < 2)
			return;

		target.drawStringDouble(6.0F, 1.0F, false, "1.DISPLAY~~~~~~~~~~~~");
		target.drawStringDouble(28.0F, 1.0F, selectedOption == 1, " 1 ");
		drawButtonBorder(target, 28, 1, 3);
		if (age < 5)
			return;

		target.drawStringDouble(6.0F, 2.0F, false, "2.CPU/TRANSMISSION ~~~~~~");
		target.drawStringDouble(32.0F, 2.0F, selectedOption == 2, " 2 ");
		drawButtonBorder(target, 32, 2, 3);
		if (age < 8)
			return;

		target.drawStringDouble(6.0F, 3.0F, false, "3.RAM CHECK ~~~~~~~~~");
		target.drawStringDouble(28.0F, 3.0F, selectedOption == 3, " 3 ");
		drawButtonBorder(target, 28, 3, 3);
		if (age < 11)
			return;

		target.drawStringDouble(6.0F, 4.0F, false, "4.ROM CHECK ~~~~~~~~~~~~~");
		target.drawStringDouble(32.0F, 4.0F, selectedOption == 4, " 4 ");
		drawButtonBorder(target, 32, 4, 3);
		if (age < 14)
			return;

		target.drawStringDouble(6.0F, 5.0F, false, "5.DIGITAL INPUT (DI)~");
		target.drawStringDouble(28.0F, 5.0F, selectedOption == 5, " 5 ");
		drawButtonBorder(target, 28, 5, 3);
		if (age < 17)
			return;

		target.drawStringDouble(6.0F, 6.0F, false, "6.DIGITAL OUTPUT (DO)~~~~");
		target.drawStringDouble(32.0F, 6.0F, selectedOption == 6, " 6 ");
		drawButtonBorder(target, 32, 6, 3);
		if (age < 21)
			return;

		target.drawStringDouble(6.0F, 7.0F, false, "7.TEST RUN ~~~~~~~~~~");
		target.drawStringDouble(28.0F, 7.0F, selectedOption == 7, " 7 ");
		drawButtonBorder(target, 28, 7, 3);
		if (age < 24)
			return;

		target.drawStringDouble(6.0F, 8.0F, false, "8.ADJUST CLOCK ~~~~~~~~~~");
		target.drawStringDouble(32.0F, 8.0F, selectedOption == 8, " 8 ");
		drawButtonBorder(target, 32, 8, 3);
		if (age < 27)
			return;

		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringSingle(19.5F, 9.0F, false, "MENU");
		drawButtonBorder(target, 19, 9, 3);

		target.drawStringDouble(36.0F, 9.0F, selectedOption != 0 && flash, "ACK");
		drawButtonBorder(target, 36, 9, 3);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "MENU FOR FIRST LEVEL TESTING");
		target.drawStringSingle(34.0F, 1.0F, false, 0x82, 0x83);
	}
}
