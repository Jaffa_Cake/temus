package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.train.CarriageN;



public class MenuTestRun extends AMenu
{
	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}

				case 19:
				case 20:
				case 21:
				{
					Program.program.menu = new MenuTesting();
					Program.program.redraw();
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return true;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "TEST RUN INFORMATION");
		target.drawStringSingle(23.0F, 0.0F, false, 0x82, 0x83);


		// Train
		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, false);
		}


		target.drawStringDouble(2.0F, 1.0F, false, "CAR");
		target.drawStringDouble(2.0F, 2.0F, false, "LS");
		target.drawStringSingle(4.0F, 2.0F, false, "(ON/OFF)");
		target.drawStringDouble(2.0F, 3.0F, false, "EC");
		target.drawStringSingle(4.0F, 3.0F, false, "(V)");
		target.drawStringDouble(2.0F, 4.0F, false, "IA1");
		target.drawStringDouble(2.0F, 5.0F, false, "IA2");
		target.drawStringDouble(2.0F, 6.0F, false, "IF1");
		target.drawStringDouble(2.0F, 7.0F, false, "IF2");
		for (int i = 0; i < 4; i++)
		{
			target.drawStringSingle(5.0F, 4.0F + (float)i, false, "(A)");
		}


		for (int i = 0; i < Program.program.train.size(); i++)
		{
			if (Program.program.train.get(i) instanceof CarriageN)
			{
				CarriageN c = (CarriageN)Program.program.train.get(i);

				boolean lineSwitch = false;
				int voltageInput = 8;
				int currentArmature = 0;
				int currentField = 0;

				target.drawStringSingle(8.5F + (i * 4.0F), 2.0F, false, lineSwitch ? " ON" : "OFF");
				target.drawStringSingle(10.0F + (i * 4.0F) + (String.valueOf(voltageInput).length() * -0.5F), 3.0F, false, String.valueOf(voltageInput));
				target.drawStringSingle(10.0F + (i * 4.0F) + (String.valueOf(currentArmature).length() * -0.5F), 4.0F, false, String.valueOf(currentArmature));
				target.drawStringSingle(10.0F + (i * 4.0F) + (String.valueOf(currentArmature).length() * -0.5F), 5.0F, false, String.valueOf(currentArmature));
				target.drawStringSingle(10.0F + (i * 4.0F) + (String.valueOf(currentField).length() * -0.5F), 6.0F, false, String.valueOf(currentField));
				target.drawStringSingle(10.0F + (i * 4.0F) + (String.valueOf(currentField).length() * -0.5F), 7.0F, false, String.valueOf(currentField));
			}
		}


		target.drawStringSingle(6.0F, 8.0F, false, "VEL.");
		target.drawStringDouble(8.0F, 8.0F, false, "~");
		target.drawStringSingle(10.0F, 8.0F, false, "0 km/h");

		target.drawStringSingle(15.0F, 8.0F, false, "LOCATION CODE");
		target.drawStringDouble(22.0F, 8.0F, false, "~");
		target.drawStringSingle(23.0F, 8.0F, false, "----");

		target.drawStringSingle(27.0F, 8.0F, false, 0x80, 0x2C, 0x81);
		target.drawStringDouble(29.0F, 8.0F, false, "~");
		target.drawStringSingle(30.0F, 8.0F, false, "+0.0 m/s/s");


		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringSingle(19.5F, 9.0F, false, "MENU");
		drawButtonBorder(target, 19, 9, 3);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "TEST RUN INFORMATION");
		target.drawStringSingle(26.0F, 1.0F, false, 0x82, 0x83);
	}
}
