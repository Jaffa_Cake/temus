package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.train.ACarriage;
import net.jaffa.temus.train.CarriageD;
import net.jaffa.temus.train.CarriageN;



public class MenuRomCheck extends AMenu
{

	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}

				case 19:
				case 20:
				case 21:
				{
					Program.program.menu = new MenuTesting();
					Program.program.redraw();
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 16 + (int)(Program.program.train.size() * 0.5F);
	}

	@Override
	public void renderFull(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");
		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "ROM CHECK");
		target.drawStringSingle(12.0F, 0.0F, false, 0x82, 0x83);

		target.drawStringDouble(2.0F, 1.0F, false, "CAR");
		target.drawStringDouble(2.0F, 2.0F, false, "DDU");

		if (age < 2)
			return;

		// Render Side
		target.drawStringDouble(2.0F, 3.0F, false, "MCU");
		target.drawStringDouble(2.0F, 4.0F, false, "CA");
		target.drawStringDouble(2.0F, 5.0F, false, "CHIF");

		// Render Legend
		target.drawStringSingle(5.0F, 8.0F, false, "[Symbol]");

		if (age < 4)
			return;

		target.drawStringSingle(12.0F, 8.0F, false, 0x84, 0x85);
		target.drawStringDouble(13.0F, 8.0F, false, ";OK");

		target.drawStringSingle(19.0F, 8.0F, false, 0x86, 0x87);
		target.drawStringDouble(20.0F, 8.0F, false, ";NG");

		target.drawStringDouble(26.0F, 8.0F, false, "-;UNKNOWN");

		if (age < 6)
			return;

		// Render Buttons
		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringSingle(19.5F, 9.0F, false, "MENU");
		drawButtonBorder(target, 19, 9, 3);

		if (age < 8)
			return;

		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, false);
			if (age < 8 + x)
				return;
		}


		// Render Train
		for (int x = 0; x < Program.program.train.size(); x++)
		{
			final int[] ok = new int[] { 0x84, 0x85 };
			final int[] ng = new int[] { 0x86, 0x87 };

			ACarriage car = Program.program.train.get(x);

			if (car instanceof CarriageD)
			{
				target.drawStringSingle(9.0F + (4.0F * x), 2.0F, false, ok);
				target.drawStringSingle(9.0F + (4.0F * x), 3.0F, false, ok);
			}
			else if (car instanceof CarriageN)
			{
				target.drawStringSingle(9.0F + (4.0F * x), 4.0F, false, ok);
				target.drawStringSingle(9.0F + (4.0F * x), 5.0F, false, ok);
			}

			if (age < 16 + (int)(x * 0.5F))
				return;
		}
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "ROM CHECK");
		target.drawStringSingle(15.0F, 1.0F, false, 0x82, 0x83);
	}
}
