package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.train.ACarriage;
import net.jaffa.temus.train.CarriageD;
import net.jaffa.temus.train.CarriageN;



public class MenuCPUTransmission extends AMenu
{
	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}

				case 19:
				case 20:
				case 21:
				{
					Program.program.menu = new MenuTesting();
					Program.program.redraw();
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 14 + ((Program.program.train.size() / 4) * 16);
	}

	private void renderCarDiagram(Bitmap target, ACarriage car, float x, float y, int age)
	{
		// All rendering should be within 16 ticks
		// Each 4 car block is offset by 16 ticks

		boolean flash = Program.program.ticks >= 30;

		int index = Program.program.train.indexOf(car);
		int ageOffset = index;
		while (ageOffset >= 4)
		{
			ageOffset -= 4;
			age -= 16;
		}

		if (car instanceof CarriageD)
		{
			CarriageD carD = (CarriageD)car;
			boolean coupled = true;

			if (car.reversed)
			{
				if (index <= 0)
				{
					coupled = false;
				}
			}
			else
			{
				if (index >= Program.program.train.size() - 1)
				{
					coupled = false;
				}
			}

			if (carD.reversed)
			{
				if (age < 2)
					return;

				drawButtonBorder(target, x + 1.0F, y + 1.0F, 2, false, true);
				target.drawStringSingle(x + 1.0F, y + 1.0F, false, "DDU");

				drawButtonBorder(target, x + 1.0F, y + 3.0F, 2, false, true);
				target.drawStringSingle(x + 1.0F, y + 3.0F, false, "MCU");

				if (age < 8)
					return;

				drawButtonBorderNoTop(target, x, y, 2);
				target.drawStringSingle(x, y, !carD.destinationBoard ? flash : false, " DES");

				if (age < 10)
					return;

				// Cables
				target.drawStringSingle(x + 3.0F, y + 3.0F, false, 0xBD, 0xBE);
				target.drawStringSingle(x + 3.0F, y + 2.0F, false, 0xBD, 0xBE);

				if (coupled)
				{
					target.drawStringSingle(x + 2.0F, y + 2.0F, false, 0x9B, 0x9C);
					target.drawStringSingle(x + 1.0F, y + 2.0F, false, 0xBD, 0xBE);
					target.drawStringSingle(x + 1.0F, y + 2.0F, false, 0xBB, 0xBC);
					target.drawStringSingle(x, y + 2.0F, false, 0xBD, 0xBE);

					if (carD.destinationBoard || !flash)
					{
						target.drawStringSingle(x, y + 3.0F, false, 0xCD, 0xCE);
						target.drawStringSingle(x, y + 2.0F, false, 0xBB, 0xBC);
						target.drawStringSingle(x, y + 1.0F, false, 0xEB, 0xEC);
					}
				}
				else
				{
					target.drawStringSingle(x + 2.0F, y + 2.0F, false, 0xDD, 0xDE);
					target.drawStringSingle(x + 1.0F, y + 2.0F, false, 0xEB, 0xEC);

					if (carD.destinationBoard || !flash)
					{
						target.drawStringSingle(x, y + 3.0F, false, 0xCD, 0xCE);
						target.drawStringSingle(x, y + 2.0F, false, 0xEB, 0xEC);
						target.drawStringSingle(x, y + 1.0F, false, 0xEB, 0xEC);
					}
				}
			}
			else
			{
				if (age < 2)
					return;

				drawButtonBorder(target, x + 1.0F, y + 1.0F, 2, true, false);
				target.drawStringSingle(x + 1.5F, y + 1.0F, false, "DDU");

				drawButtonBorder(target, x + 1.0F, y + 3.0F, 2, true, false);
				target.drawStringSingle(x + 1.5F, y + 3.0F, false, "MCU");

				if (age < 8)
					return;

				drawButtonBorderNoTop(target, x + 2.0F, y, 2);
				target.drawStringSingle(x + 2.0F, y, !carD.destinationBoard ? flash : false, "DES ");

				if (age < 10)
					return;

				// Cables
				target.drawStringSingle(x, y + 3.0F, false, 0xBD, 0xBE);
				target.drawStringSingle(x, y + 2.0F, false, 0xBD, 0xBE);

				if (coupled)
				{
					target.drawStringSingle(x + 1.0F, y + 2.0F, false, 0x9B, 0x9C);
					target.drawStringSingle(x + 2.0F, y + 2.0F, false, 0xBD, 0xBE);
					target.drawStringSingle(x + 2.0F, y + 2.0F, false, 0xBB, 0xBC);
					target.drawStringSingle(x + 3.0F, y + 2.0F, false, 0xBD, 0xBE);

					if (carD.destinationBoard || !flash)
					{
						target.drawStringSingle(x + 3.0F, y + 3.0F, false, 0xCB, 0xCC);
						target.drawStringSingle(x + 3.0F, y + 2.0F, false, 0xBB, 0xBC);
						target.drawStringSingle(x + 3.0F, y + 1.0F, false, 0xEB, 0xEC);
					}
				}
				else
				{
					target.drawStringSingle(x + 1.0F, y + 2.0F, false, 0xDB, 0xDC);
					target.drawStringSingle(x + 2.0F, y + 2.0F, false, 0xEB, 0xEC);

					if (carD.destinationBoard || !flash)
					{
						target.drawStringSingle(x + 3.0F, y + 3.0F, false, 0xCB, 0xCC);
						target.drawStringSingle(x + 3.0F, y + 2.0F, false, 0xEB, 0xEC);
						target.drawStringSingle(x + 3.0F, y + 1.0F, false, 0xEB, 0xEC);
					}
				}
			}
		}
		else if (car instanceof CarriageN)
		{
			CarriageN carN = (CarriageN)car;

			if (age < 4)
				return;

			drawButtonBorder(target, x + 1.0F, y + 3.0F, 2, true, true);
			target.drawStringSingle(x + 1.5F, y + 3.0F, false, "CA");

			if (age < 6)
				return;

			drawButtonBorder(target, x + 1.0F, y + 5.0F, 2, false, false);
			target.drawStringSingle(x + 1.0F, y + 5.0F, false, "CHIF");

			if (age < 12)
				return;

			if (carN.reversed)
			{
				// Cables
				target.drawStringSingle(x + 3.0F, y + 3.0F, false, 0xBD, 0xBE);
			}
			else
			{
				// Cables
				target.drawStringSingle(x, y + 3.0F, false, 0xBD, 0xBE);
			}

			// Cables
			target.drawStringSingle(x + 1.5F, y + 4.0F, false, 0xEB, 0xEC);
			target.drawStringSingle(x, y + 2.0F, false, 0xBD, 0xBE, 0xBD, 0xBE, 0xBD, 0xBE, 0xBD, 0xBE);
		}
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "CPU/TRANSMISSION CHECK");
		target.drawStringSingle(25.0F, 0.0F, false, 0x82, 0x83);

		target.drawStringDouble(2.0F, 1.0F, false, "CAR");

		if (age < 2)
			return;

		// Render Legend
		target.drawStringSingle(5.0F, 8.0F, false, "[Symbol]");

		drawButtonBorder(target, 11.0F, 8.0F, 2, false, true);
		target.drawStringDouble(13.0F, 8.0F, false, ",");
		target.drawStringDouble(14.0F, 8.0F, false, 0xB6, 0xB6);
		target.drawStringDouble(16.0F, 8.0F, false, ";OK");

		if (age < 4)
			return;

		if (flash)
		{
			drawButtonBorder(target, 21.0F, 8.0F, 2, true, true);
			target.drawStringDouble(21.0F, 8.0F, true, "  ");
		}
		else
		{
			target.drawStringDouble(24.0F, 8.0F, false, 0xB6, 0xB6);
		}
		target.drawStringDouble(23.0F, 8.0F, false, ",");
		target.drawStringDouble(26.0F, 8.0F, false, ";NG");

		// Render Buttons
		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringSingle(19.5F, 9.0F, false, "MENU");
		drawButtonBorder(target, 19, 9, 3);

		if (age < 6)
			return;

		// Render Train & Diagram
		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, false);
			if (age < 6 + x)
				return;
		}

		for (int x = 0; x < Program.program.train.size(); x++)
		{
			renderCarDiagram(target, Program.program.train.get(x), 7.0F + (4.0F * x), 2.0F, age - 14);
		}
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "CPU/TRANSMISSION CHECK");
		target.drawStringSingle(28.0F, 1.0F, false, 0x82, 0x83);
	}
}
