package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuOperationInformation extends AMenu
{

	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}

				case 19:
				case 20:
				case 21:
				{
					Program.program.menu = new MenuOtherInformation();
					Program.program.redraw();
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 10 + Program.program.train.size();
	}

	@Override
	public void renderFull(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "OPERATION INFORMATION");
		target.drawStringSingle(24.0F, 0.0F, false, 0x82, 0x83);

		if (age < 2)
			return;

		target.drawStringDouble(2.0F, 1.0F, false, "CAR");
		target.drawStringDouble(2.0F, 2.0F, false, "DATE");
		target.drawStringDouble(2.0F, 3.0F, false, "TIME");

		// Date
		if (age >= 18)
		{
			String day = String.valueOf(Program.program.dateNow.getDayOfMonth());
			String month = String.valueOf(Program.program.dateNow.getMonthValue());
			String year = String.valueOf(Program.program.dateNow.getYear());
			if (day.length() < 2)
				day = " " + day;
			if (month.length() < 2)
				month = " " + month;
			target.drawStringDouble(9.0F, 2.0F, false, day + "." + month + "." + year);
		}
		else
		{
			target.drawStringDouble(9.0F, 2.0F, false, "00.00.0000");
		}

		// Time
		if (age >= 18)
		{
			String hour = String.valueOf(Program.program.timeNow.getHour());
			String minute = String.valueOf(Program.program.timeNow.getMinute());
			String second = String.valueOf(Program.program.timeNow.getSecond());
			if (hour.length() < 2)
				hour = " " + hour;
			if (minute.length() < 2)
				minute = "0" + minute;
			if (second.length() < 2)
				second = "0" + second;
			target.drawStringDouble(9.0F, 3.0F, false, hour + ":" + minute + "'" + second + "\"");
		}
		else
		{
			target.drawStringDouble(11.0F, 3.0F, false, ":");
		}

		if (age < 4)
			return;

		target.drawStringDouble(2.0F, 4.0F, false, "NOTCH");
		if (age >= 18)
		{
			target.drawStringDouble(9.0F, 4.0F, false, "C");
		}
		else
		{
			target.drawStringDouble(9.0F, 4.0F, false, "P"); // Interim
		}

		//		target.drawStringDouble(2.0F, 5.0F, false, "VEL.");

		target.drawStringDouble(2.0F, 6.0F, false, "LOC.");
		target.drawStringSingle(14.0F, 6.0F, false, "(CODE)");

		if (age >= 18)
		{
			target.drawStringDouble(9.0F, 6.0F, false, "----");
		}

		if (age < 6)
			return;

		target.drawStringDouble(2.0F, 7.0F, false, "ODOMT");
		target.drawStringSingle(16.0F, 7.0F, false, "km");

		// Odometer
		if (age >= 20)
		{
			target.drawStringDouble(16.0F - String.valueOf(Program.program.odometer).length(), 7.0F, false, String.valueOf(Program.program.odometer));
		}

		if (age < 8)
			return;


		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringSingle(19.5F, 9.0F, false, "MENU");
		drawButtonBorder(target, 19, 9, 3);

		if (age < 10)
			return;

		// Train
		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, false);
			if (age < 10 + x)
				return;
		}
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "OPERATION INFORMATION");
		target.drawStringSingle(27.0F, 1.0F, false, 0x82, 0x83);
	}
}
