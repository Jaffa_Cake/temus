package net.jaffa.temus.menu;

import com.sun.org.apache.xpath.internal.operations.String;
import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuCredits extends AMenu
{
	private boolean unlocked = false;

	public MenuCredits()
	{
	}

	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y >= 7 && y <= 9 && x >= 7 && x <= 13 && unlocked)
		{
			randomiseColour();
			return true;
		}

		else if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}
			}
		}
		return false;
	}


	@Override
	public boolean activateHard(int x, int y)
	{
		if (y >= 7 && y <= 9 && x >= 7 && x <= 13)
		{
			randomiseColour();
			unlocked = true;
			return true;
		}
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return true;
	}

	private void randomiseColour()
	{
		Program.COLOUR = 0xFF000000 + (Program.random.nextInt(0xFF) << 16) + (Program.random.nextInt(0xFF) << 8) + (Program.random.nextInt(0xFF));
	}

	@Override
	public void renderFull(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "TEMUS CREDITS");
		target.drawStringSingle(16.0F, 0.0F, false, 0x82, 0x83);

		target.drawStringDouble(5.0F, 5.0F, true, " T E M U S ");
		target.drawStringSingle(4.5F, 5.0F, false, 0x86);
		target.drawStringSingle(16.0F, 5.0F, false, 0x87);
		target.drawStringSingle(5.5F, 6.0F, false, "TANGARA TMS EMULATOR");
		target.drawStringSingle(6.5F, 8.0F, false, "Made by Jaffa :)");

		target.drawStringSingle(8.0F, 2.0F, false, 0x98, 0x99, 0x9A);
		target.drawStringSingle(7.5F, 3.0F, false, 0xA8, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xA9, 0xAA);
		target.drawStringSingle(11.5F, 4.0F, false, 0xB8, 0xB9, 0xBA);


		target.drawStringSingle(20.0F, 2.0F, false, "TEMUS is an accurate-to-life Tangara ");
		target.drawStringSingle(20.0F, 3.0F, false, "Train Management System emulator. The");
		target.drawStringSingle(20.0F, 4.0F, false, "pixel art, font, and behaviour of the");
		target.drawStringSingle(20.0F, 5.0F, false, "emulator has been copied as closely  ");
		target.drawStringSingle(20.0F, 6.0F, false, "as possible from videos and photos of");
		target.drawStringSingle(20.0F, 7.0F, false, "the real system.                     ");
		target.drawStringSingle(20.0F, 8.0F, false, "                    v0.2 ~ 04/04/2023");

		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "TEMUS CREDITS");
		target.drawStringSingle(19.0F, 1.0F, false, 0x82, 0x83);
	}

}
