package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuOtherInformation extends AMenu
{
	private int selectedOption;

	public MenuOtherInformation()
	{
		selectedOption = 0;
	}

	@Override
	public boolean activateSoft(int x, int y)
	{
		switch (x)
		{
			case 1:
			case 2:
			case 3:
			{
				if (y == 14) // CLR
				{
					Program.program.menu = new MenuBlank();
					Program.program.redraw();
					return true;
				}
				break;
			}

			case 36:
			case 37:
			case 38:
			{
				if (y == 14) // ACK
				{
					switch (selectedOption)
					{
						case 1:
						{
							Program.program.menu = new MenuOperationInformation();
							Program.program.redraw();
							return true;
						}

						case 2:
						{
							Program.program.menu = new MenuCutoutInformation();
							Program.program.redraw();
							return true;
						}
					}
				}
				break;
			}
		}

		switch (y)
		{
			case 7:
			{
				if (x >= 28 && x <= 30)
				{
					selectedOption = 1;
					return true;
				}
				break;
			}

			case 8:
			{
				if (x >= 32 && x <= 34)
				{
					selectedOption = 2;
					return true;
				}
				break;
			}

			case 9:
			{
				if (x >= 28 && x <= 30)
				{
					selectedOption = 3;
					return true;
				}
				break;
			}

			case 10:
			{
				if (x >= 32 && x <= 34)
				{
					selectedOption = 4;
					return true;
				}
				break;
			}
		}

		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		if (x >= 19 && x <= 21 && y == 14)
		{
			Program.program.menu = new MenuTesting();
			Program.program.redraw();
			return true;
		}

		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 15;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "OTHER INFORMATION");
		target.drawStringSingle(20.0F, 0.0F, false, 0x82, 0x83);

		if (age < 2)
			return;

		target.drawStringDouble(2.0F, 2.0F, false, "1.OPERATION INFORMATION ~");
		target.drawStringDouble(28.0F, 2.0F, selectedOption == 1, " 1 ");
		drawButtonBorder(target, 28, 2, 3, false, true);
		if (age < 5)
			return;

		target.drawStringDouble(2.0F, 3.0F, false, "2.CUTOUT INFORMATION ~~~~~~~~");
		target.drawStringDouble(32.0F, 3.0F, selectedOption == 2, " 2 ");
		drawButtonBorder(target, 32, 3, 3, false, true);
		if (age < 8)
			return;

		target.drawStringDouble(2.0F, 4.0F, false, "3.MEMORY INFORMATION ~~~~");
		target.drawStringDouble(28.0F, 4.0F, selectedOption == 3, " 3 ");
		drawButtonBorder(target, 28, 4, 3, false, true);
		if (age < 11)
			return;

		target.drawStringDouble(2.0F, 5.0F, false, "4.SECURITY TEST INFORMATION ~");
		target.drawStringDouble(32.0F, 5.0F, selectedOption == 4, " 4 ");
		drawButtonBorder(target, 32, 5, 3, false, true);
		if (age < 14)
			return;

		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		target.drawStringDouble(19.0F, 9.0F, false, 0xA6, 0xA6, 0xA6);
		drawButtonBorder(target, 19, 9, 3);

		target.drawStringDouble(36.0F, 9.0F, selectedOption != 0 && flash, "ACK");
		drawButtonBorder(target, 36, 9, 3);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "OTHER INFORMATION");
		target.drawStringSingle(23.0F, 1.0F, false, 0x82, 0x83);
	}
}
