package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;
import net.jaffa.temus.train.ACarriage;
import net.jaffa.temus.train.CarriageD;
import net.jaffa.temus.train.CarriageN;



public class MenuCutoutInformation extends AMenu
{
	private boolean startup;
	public int page;


	public MenuCutoutInformation()
	{
		this(false);
	}

	public MenuCutoutInformation(boolean startup)
	{
		this.startup = startup;

		page = 0;
	}

	@Override
	public boolean activateSoft(int x, int y)
	{
		if (y == 14)
		{
			switch (x)
			{
				case 1:
				case 2:
				case 3:
				{
					if (startup)
					{
						Program.program.menu = new MenuFaultList(true);
					}
					else
					{
						Program.program.menu = new MenuBlank();
					}
					Program.program.redraw();
					return true;
				}

				case 19:
				case 20:
				case 21:
				{
					if (startup)
					{
						return false;
					}
					else
					{
						Program.program.menu = new MenuOtherInformation();
						Program.program.redraw();
						return true;
					}
				}

				case 36:
				case 37:
				case 38:
				{
					page++;
					if (page > 1)
					{
						page = 0;
					}
					Program.program.redraw();
					Program.program.redraw = Program.WIDTH * (Program.HEIGHT / 3);
					age = 0;
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 22 + Program.program.train.size();
	}

	@Override
	public void renderFull(Bitmap target)
	{
		final boolean flash = Program.program.ticks >= 30;

		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(0.0F, 0.0F, false, 0x82, 0x83);
		target.drawStringDouble(2.0F, 0.0F, false, "CUTOUT INFORMATION");
		target.drawStringSingle(21.0F, 0.0F, false, 0x82, 0x83);

		switch (page)
		{
			case 0:
			{
				target.drawStringDouble(2.0F, 1.0F, false, "CAR");
				if (age < 2)
					return;
				target.drawStringDouble(2.0F, 2.0F, false, "CH-M1");
				target.drawStringDouble(2.0F, 3.0F, false, "CH-M2");
				target.drawStringDouble(2.0F, 4.0F, false, "SIV");
				target.drawStringDouble(2.0F, 5.0F, false, "EP.BR");
				if (age < 4)
					return;
				target.drawStringDouble(2.0F, 6.0F, false, "BOG.B");
				target.drawStringDouble(2.0F, 7.0F, false, "COMP");
				break;
			}

			case 1:
			{
				target.drawStringDouble(2.0F, 1.0F, false, "CAR");
				if (age < 2)
					return;
				target.drawStringDouble(2.0F, 2.0F, false, "ATC");
				target.drawStringDouble(2.0F, 3.0F, false, "ACON");
				target.drawStringDouble(2.0F, 4.0F, false, "DOOR");
				target.drawStringSingle(2.0F, 5.0F, false, "M-LIGHT");
				if (age < 4)
					return;
				target.drawStringSingle(2.0F, 6.0F, false, "E-LIGHT");
				break;
			}
		}

		target.drawStringSingle(5.0F, 8.0F, false, "[Symbol]");
		if (age < 6)
			return;

		target.drawStringSingle(12.0F, 8.0F, false, 0x86, 0x87);
		target.drawStringDouble(13.0F, 8.0F, false, ";");
		target.drawStringSingle(14.0F, 8.0F, false, "CUTOUT");

		target.drawStringSingle(20.0F, 8.0F, false, 0x84, 0x85);
		target.drawStringDouble(21.0F, 8.0F, false, ";");
		target.drawStringSingle(22.0F, 8.0F, false, "CUTIN");
		if (age < 8)
			return;

		target.drawStringDouble(28.0F, 8.0F, false, "-;");
		target.drawStringSingle(30.0F, 8.0F, false, "UNKNOWN");


		target.drawStringDouble(1.0F, 9.0F, false, "CLR");
		drawButtonBorder(target, 1, 9, 3);

		if (!startup)
		{
			target.drawStringSingle(19.5F, 9.0F, false, "MENU");
			drawButtonBorder(target, 19, 9, 3);
		}

		target.drawStringDouble(36.0F, 9.0F, flash, "ACK");
		drawButtonBorder(target, 36, 9, 3);
		if (age < 10)
			return;

		// Train
		for (int x = 0; x < Program.program.train.size(); x++)
		{
			Program.program.train.get(x).render(target, 7.0F + (4.0F * x), 1.0F, false);
			if (age < 12 + x)
				return;
		}

		final int[] in = new int[] { 0x84, 0x85 };
		final int[] out = new int[] { 0x86, 0x87 };
		switch (page)
		{
			case 0:
			{
				for (int x = 0; x < Program.program.train.size(); x++)
				{
					if (age < 22 + x)
						return;

					ACarriage car = Program.program.train.get(x);

					if (car instanceof CarriageD)
					{
						CarriageD c = (CarriageD)car;

						target.drawStringSingle(9.0F + (x * 4.0F), 4.0F, false, c.siv ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 5.0F, false, c.epBrake ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 6.0F, false, c.bogieBrake ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 7.0F, false, c.compressor ? in : out);
					}
					else if (car instanceof CarriageN)
					{
						CarriageN c = (CarriageN)car;

						target.drawStringSingle(9.0F + (x * 4.0F), 2.0F, false, c.motor1 ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 3.0F, false, c.motor2 ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 6.0F, false, c.bogieBrake ? in : out);
					}
				}
				break;
			}

			case 1:
			{
				for (int x = 0; x < Program.program.train.size(); x++)
				{
					if (age < 22 + x)
						return;

					ACarriage car = Program.program.train.get(x);

					if (car instanceof CarriageD)
					{
						CarriageD c = (CarriageD)car;

						target.drawStringSingle(9.0F + (x * 4.0F), 2.0F, false, c.atc ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 3.0F, false, c.aircon ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 4.0F, false, c.doors ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 5.0F, false, c.mainLights ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 6.0F, false, c.emergencyLights ? in : out);
					}
					else if (car instanceof CarriageN)
					{
						CarriageN c = (CarriageN)car;

						target.drawStringSingle(9.0F + (x * 4.0F), 3.0F, false, c.aircon ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 4.0F, false, c.doors ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 5.0F, false, c.mainLights ? in : out);
						target.drawStringSingle(9.0F + (x * 4.0F), 6.0F, false, c.emergencyLights ? in : out);
					}
				}
				break;
			}
		}
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TMS");

		target.drawStringSingle(3.0F, 1.0F, false, 0x82, 0x83);
		target.drawStringDouble(5.0F, 1.0F, false, "CUTOUT INFORMATION");
		target.drawStringSingle(24.0F, 1.0F, false, 0x82, 0x83);
	}
}
