package net.jaffa.temus.menu;

import net.jaffa.temus.Program;
import net.jaffa.temus.graphics.Bitmap;



public class MenuTrainRadio extends AMenu
{

	@Override
	public boolean activateSoft(int x, int y)
	{
		return false;
	}

	@Override
	public boolean activateHard(int x, int y)
	{
		if (y == 13)
		{
			switch (x)
			{
				case 33:
				case 34:
				case 35:
				{
					Program.program.menu = new MenuSpritesheet();
					Program.program.radioFocused = false;
					Program.program.redraw();
					return true;
				}

				case 36:
				case 37:
				case 38:
				{
					Program.program.menu = new MenuCredits();
					Program.program.radioFocused = false;
					Program.program.redraw();
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isFinishedRendering()
	{
		return age >= 6;
	}

	@Override
	public void renderFull(Bitmap target)
	{
		drawMenuIndicator(target, "TR");

		for (float x = 5.5F; x <= 29.0F; x += 0.5F)
		{
			target.drawStringSingle(x, 1.0F, false, 0xB2);
			target.drawStringSingle(x, 5.0F, false, 0xB2);
		}
		for (float y = 2.0F; y <= 4.0F; y += 1.0F)
		{
			target.drawStringSingle(5.0F, y, false, 0xC2);
			target.drawStringSingle(29.5F, y, false, 0xC2);
		}
		target.drawStringSingle(5.0F, 1.0F, false, 0xB0);
		target.drawStringSingle(5.0F, 5.0F, false, 0xC0);
		target.drawStringSingle(29.5F, 1.0F, false, 0xB1);
		target.drawStringSingle(29.5F, 5.0F, false, 0xC1);


		for (int x = 0; x < 10; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				if (x >= 1 && x <= 7 && y <= 1)
				{
					continue;
				}
				target.drawStringDouble(0.0F + (x * 4.0F), 2.0F + (y * 3), true, 0xA7);
			}
		}

		target.drawStringSingle(1.0F, 2.0F, true, 0x8C, 0x8D, 0x8C, 0x8D);
		target.drawStringSingle(1.0F, 5.0F, true, 0x8A, 0x8B, 0x8A, 0x8B);
		target.drawStringDouble(1.0F, 8.0F, true, "MG");
		target.drawStringDouble(5.0F, 8.0F, true, "AK");
		target.drawStringDouble(9.0F, 8.0F, true, "CC");
		target.drawStringDouble(13.0F, 8.0F, true, "FN");
		target.drawStringDouble(17.0F, 8.0F, true, "T ");
		target.drawStringDouble(21.0F, 8.0F, true, "  ");
		target.drawStringDouble(25.0F, 8.0F, true, "AR");
		target.drawStringDouble(29.0F, 8.0F, true, "RG");
		target.drawStringDouble(33.0F, 8.0F, true, "C ");
		target.drawStringDouble(37.0F, 8.0F, true, "CS");
		target.drawStringDouble(33.0F, 5.0F, true, "* ");
		target.drawStringDouble(37.0F, 5.0F, true, "# ");
		target.drawStringSingle(33.0F, 2.0F, true, 0x88, 0x89, 0x88, 0x89);
		target.drawStringSingle(37.0F, 2.0F, true, 0x8E, 0x8F, 0x8E, 0x8F);
	}

	@Override
	public void renderMinimised(Bitmap target)
	{
		drawMenuIndicator(target, "TR");

		target.drawStringSingle(5.0F, 3.0F, false, 0xC0);
		target.drawStringSingle(29.5F, 3.0F, false, 0xC1);
		for (float x = 5.5F; x <= 29.0F; x += 0.5F)
		{
			target.drawStringSingle(x, 3.0F, false, 0xB2);
		}

		if (age < 2)
			return;

		target.drawStringSingle(5.0F, 0.0F, false, 0xB0);
		target.drawStringSingle(29.5F, 0.0F, false, 0xB1);
		for (float x = 5.5F; x <= 29.0F; x += 0.5F)
		{
			target.drawStringSingle(x, 0.0F, false, 0xB2);
		}

		if (age < 4)
			return;

		for (float y = 1.0F; y <= 2.0F; y += 1.0F)
		{
			target.drawStringSingle(5.0F, y, false, 0xC2);
			target.drawStringSingle(29.5F, y, false, 0xC2);
		}
	}
}
